# Solutions to TopCoder SRM 564 problems

This repository contains solutions to [TopCoder][1] problems from SRM 564. These solutions are provided "as is". I give no guarantees that they will work as expected.

Each problem is provided as an Eclipse project with the problem statement as HTML, a solution source file and a source file with JUnit tests (containing, at least, the test cases in the problem statement). The projects were created using the [EclipseCoder][2] plugin.

## Problems status

Here is a list of the problems currently in this repository. Problems marked with ✓ are done, while problems with ✗ are not complete and/or aren't passing one or more unit tests.

* ✓ Div1 250 (`knightcircuit2-java`)
* ✓ Div1 500 (`alternatecolors2-java`)
* ✗ Div1 850 (`defectiveaddition-java`)

[1]: http://www.topcoder.com/tc
[2]: http://fornwall.net/eclipsecoder