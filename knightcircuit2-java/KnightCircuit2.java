public class KnightCircuit2 {

	public int maxSize(int w, int h) {
		if (w < h) {
			int temp = w;
			w = h;
			h = temp;
		}
		if (h == 1)
			return 1;
		if (h == 2)
			return (w + 1) / 2;
		if (h == 3 && w == 3)
			return 8;
		return w * h;
	}
}
