import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DefectiveAdditionTest {

    protected DefectiveAddition solution;

    @Before
    public void setUp() {
        solution = new DefectiveAddition();
    }

    @Test
    public void testCase0() {
        int[] cards = new int[]{2, 3};
        int n = 2;

        int expected = 3;
        int actual = solution.count(cards, n);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        int[] cards = new int[]{1, 2, 3};
        int n = 1;

        int expected = 6;
        int actual = solution.count(cards, n);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        int[] cards = new int[]{4, 5, 7, 11};
        int n = 6;

        int expected = 240;
        int actual = solution.count(cards, n);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        int[] cards = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int n = 15;

        int expected = 1965600;
        int actual = solution.count(cards, n);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        int[] cards = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int n = 16;

        int expected = 0;
        int actual = solution.count(cards, n);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase5() {
        int[] cards = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        int n = 1;

        int expected = 949480669;
        int actual = solution.count(cards, n);

        Assert.assertEquals(expected, actual);
    }

}
