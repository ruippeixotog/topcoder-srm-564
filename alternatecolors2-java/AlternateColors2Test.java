import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AlternateColors2Test {

    protected AlternateColors2 solution;

    @Before
    public void setUp() {
        solution = new AlternateColors2();
    }

    @Test
    public void testCase0() {
        int n = 1;
        int k = 1;

        long expected = 1L;
        long actual = solution.countWays(n, k);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        int n = 3;
        int k = 3;

        long expected = 3L;
        long actual = solution.countWays(n, k);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        int n = 6;
        int k = 4;

        long expected = 9L;
        long actual = solution.countWays(n, k);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        int n = 6;
        int k = 1;

        long expected = 21L;
        long actual = solution.countWays(n, k);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        int n = 1000;
        int k = 2;

        long expected = 1L;
        long actual = solution.countWays(n, k);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase5() {
        int n = 100000;
        int k = 100000;

        long expected = 1666700000L;
        long actual = solution.countWays(n, k);

        Assert.assertEquals(expected, actual);
    }

}
