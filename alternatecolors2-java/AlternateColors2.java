public class AlternateColors2 {

	public long countWays(int n, int k) {
		k--;
		
		long count = 0;
		for (int a = 0; a <= k / 3; a++) {
			int rem = k - 3 * a;
			
			count += remaining(n, k, a, 0, rem);
			if(rem % 2 == 1) {
				count += 2 * (rem / 2) * remaining(n, k, a, 1, 1);
			} else if(rem != 0) {
				count += 2 * remaining(n, k, a, 1, 0);
				count += 2 * (rem / 2 - 1) * remaining(n, k, a, 1, 1);
			}
		}
		return count;
	}

	public long remaining(int n, int k, int a, int b, int c) {
		int rem = n - k - 1;
		if (c > 0)
			return 1;
		if (b > 0)
			return rem + 1;
		return (long) (rem + 1) * (rem + 2) / 2;
	}
}
